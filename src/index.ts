/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `wrangler dev src/index.ts` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `wrangler publish src/index.ts --name my-worker` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

export interface Env {
    // Example binding to KV. Learn more at https://developers.cloudflare.com/workers/runtime-apis/kv/
    // MY_KV_NAMESPACE: KVNamespace
    kv: KVNamespace;
    //
    // Example binding to Durable Object. Learn more at https://developers.cloudflare.com/workers/runtime-apis/durable-objects/
    // MY_DURABLE_OBJECT: DurableObjectNamespace;
    //
    // Example binding to R2. Learn more at https://developers.cloudflare.com/workers/runtime-apis/r2/
    // MY_BUCKET: R2Bucket;
}

type FileMetadataT = {
    date: Date | string,
    headers: [string, string][],
};

function toHeaders(metadata?: FileMetadataT | null): [key: string, value: string][] {
    return metadata && [
        ['X-Hit', 'true'],
        ['X-Upload-Date', JSON.parse(JSON.stringify(new Date(metadata.date)))],
        ...metadata.headers.map(([k, v]) => [`X-Upload-Header-${k}`, v] as [string, string]),
    ] || [
        ['X-Hit', 'false'],
    ]
}

async function updateList(env: Env): Promise<KVNamespaceListResult<FileMetadataT>> {
    const result = await env.kv.list<FileMetadataT>({prefix: 'files:'})
    await env.kv.put('files', JSON.stringify(result), {
        metadata: {
            date: new Date(),
        },
    })
    return result
}

export default {
    async fetch(
        request: Request,
        env: Env,
        ctx: ExecutionContext,
    ): Promise<Response> {
        if (request.method === 'OPTIONS') {
            return new Response(null, {
                headers: {
                    'Access-Control-Allow-Headers': '*',
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Methods': '*',
                },
            })
        }

        const url = new URL(request.url)
        if (url.pathname === '/') {
            switch (request.method) {
                case 'GET': {
                    const idRegex = new RegExp(`^${'files'.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')}:(.*):file$`)
                    return new Response(JSON.stringify({
                        files: (await env.kv.get<KVNamespaceListResult<FileMetadataT>>('files', 'json') || await updateList(env)).keys.map(o => {
                            const match = o.name.match(idRegex)
                            if (match) {
                                return ({...o, name: match[1]})
                            }
                        }).filter(e => e),
                        latest: {
                            file: await (async () => {
                                const id = await env.kv.get('gitlab-pipeline:file', 'text')
                                if (!id) return
                                const match = idRegex.exec(id)
                                if (!match) return
                                return match[1]
                            })(),
                            pipeline: await env.kv.get('gitlab-pipeline:result', 'json') || await (async () => {
                                let pipeline = await env.kv.get('gitlab-pipeline:pipeline', 'text')
                                if (!pipeline) {
                                    return {error: "Pipeline ID unknown"}
                                }
                                let token = await env.kv.get('gitlab-pipeline:token')
                                if (!token) {
                                    return {error: "No token specified. Please store a PAT under key " + 'gitlab-pipeline:token'}
                                }
                                let project = await env.kv.get('gitlab-pipeline:project', 'text')
                                let host = await env.kv.get('gitlab-pipeline:host', 'text')
                                const res = await fetch(`https://${host}/api/v4/projects/${project}/pipelines/${pipeline}`, {
                                    headers: {
                                        'PRIVATE_TOKEN': token,
                                    },
                                })
                                const json = await res.json() as any
                                // const finalStatuses = "created, waiting_for_resource, preparing, pending, running, success, failed, canceled, skipped, manual, scheduled"
                                const finalStatuses = "success, failed, canceled, skipped"
                                if (finalStatuses.split(/, /g).includes(json.status)) {
                                    await env.kv.put('gitlab-pipeline:result', JSON.stringify(json), {
                                        metadata: {
                                            date: new Date(),
                                        },
                                    })
                                }
                                return json
                            })(),
                        },
                    }), {
                        headers: {
                            'Content-Type': 'application/json',
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Origin': '*',
                        },
                    })
                }

                case 'DELETE': {
                    await env.kv.delete('files');
                    await env.kv.delete('gitlab-pipeline:result');
                    return new Response("Status caches flushed\n", {
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Headers': '*',
                        }
                    });
                }

                default: {
                    return new Response("Method on this endpoint is not allowed\n", {
                        status: 406,
                        headers: {
                            'Content-Type': 'text/plain',
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Origin': '*',
                        },
                    })
                }
            }
        }

        const id = `files:${url.pathname}?${url.search}:file`

        switch (request.method) {
            case 'HEAD': {
                const {keys: [key]} = await env.kv.list<FileMetadataT>({prefix: id, limit: 1})
                if (key) {
                    return new Response(null, {
                        headers: [
                            ...toHeaders(key.metadata),
                            ['X-Name', key.name],
                            ['X-Expiration', key.expiration && JSON.parse(JSON.stringify(new Date(key.expiration))) || 'no'],
                            ['X-Id', id],
                            ['Access-Control-Allow-Headers', '*'],
                            ['Access-Control-Allow-Origin', '*'],
                        ],
                    })
                } else {
                    return new Response("Not found\n", {
                        status: 404,
                        headers: {
                            'Content-Type': 'text/plain',
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Origin': '*',
                        },
                    })
                }
            }

            case 'GET': {
                const {value, metadata} = await env.kv.getWithMetadata<FileMetadataT>(id, {type: 'stream'})
                return new Response(value, {
                    status: value ? 200 : 404,
                    headers: [
                        ...toHeaders(metadata),
                        ['X-Id', id],
                        ['Access-Control-Allow-Headers', '*'],
                        ['Access-Control-Allow-Origin', '*'],
                    ],
                })
            }

            case 'PUT': {
                if (!request.body) {
                    return new Response("No request body\n", {
                        status: 403,
                        headers: {
                            'Content-Type': 'text/plain',
                            'Access-Control-Allow-Headers': '*',
                            'Access-Control-Allow-Origin': '*',
                        },
                    })
                }
                await env.kv.put(id, request.body, {
                    metadata: {
                        date: new Date(),
                        headers: [...request.headers],
                    } as FileMetadataT,
                })
                await updateList(env)
                const triggerUrl = await env.kv.get('gitlab-trigger:url', 'text')
                const triggerToken = await env.kv.get('gitlab-trigger:token', 'text')
                const triggerRef = await env.kv.get('gitlab-trigger:ref', 'text')
                if (triggerUrl && triggerToken && triggerRef) {
                    const res = await fetch(triggerUrl, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/x-www-form-urlencoded',
                        },
                        body: new URLSearchParams({
                            token: triggerToken,
                            ref: triggerRef,
                            'variables[XLSX_URL]': `https://${request.headers.get('host')}/${url.pathname}?${url.search}`,
                            'variables[XLSX_ID]': id,
                        }),
                    })
                    const result = await res.json() as any
                    console.log(result)
                    const {id: pipeline_id, project_id} = result
                    await env.kv.delete('gitlab-pipeline:pipeline')
                    await env.kv.delete('gitlab-pipeline:result')
                    await env.kv.delete('gitlab-pipeline:file')
                    if (pipeline_id && project_id) {
                        await env.kv.put('gitlab-pipeline:host', (new URL(triggerUrl)).host)
                        await env.kv.put('gitlab-pipeline:project', project_id)
                        await env.kv.put('gitlab-pipeline:pipeline', pipeline_id)
                        await env.kv.put('gitlab-pipeline:file', id)
                    }
                }
                return new Response("Uploaded\n", {
                    status: 201,
                    headers: [
                        ['X-Id', id],
                        ['Content-Type', 'text/plain'],
                        ['Access-Control-Allow-Headers', '*'],
                        ['Access-Control-Allow-Origin', '*'],
                    ],
                })
            }

            case 'DELETE': {
                await env.kv.delete(id)
                await updateList(env)
                return new Response("Deleted\n", {
                    headers: [
                        ['X-Id', id],
                        ['Content-Type', 'text/plain'],
                        ['Access-Control-Allow-Headers', '*'],
                        ['Access-Control-Allow-Origin', '*'],
                    ],
                })
            }

            default: {
                return new Response("Unexpected method\n", {
                    status: 406,
                    headers: {
                        'Content-Type': 'text/plain',
                        'Access-Control-Allow-Headers': '*',
                        'Access-Control-Allow-Origin': '*',
                    },
                })
            }
        }
    },
}
